# Wordpress JWT Example

Wordpress plugin to demonstrate how to use the [JWT Plugin](https://wordpress.org/plugins/jwt-authentication-for-wp-rest-api/).


## How to Use

To use the JWT Plugin you've the define **JWT_AUTH_SECRET_KEY** constant in **wp-config.php**:

    define('JWT_AUTH_SECRET_KEY', 'change-this-to-a-top-secret-key');

This plugin will render a simple form and append the result of the authentication as shown below:

![Screenshot](screenshot.png?raw=true)

To authenticate the requests the JWT plugin intercepts the HTTP **Authorization** header and decode the token.

    POST /resource HTTP/1.1
    Host: gui.com.pt
    Authorization: Bearer the-token


## License

[![License](https://img.shields.io/:license-mit-blue.svg?style=flat-square)](http://badges.mit-license.org)

- **[MIT license](http://opensource.org/licenses/mit-license.php)**
- Copyright 2019 © <a href="https://gui.com.pt" target="_blank">Gui</a>.
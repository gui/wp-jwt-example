<?php
/*
Plugin Name: JWT Authentication Example
Plugin URI: https://gui.com.pt/
Description: Demonstration how to use the JWT Plugin
Version: 0.0.1
Author: Gui
Author URI: https://gui.com.pt/
Text Domain: wp-jwt-example
*/

add_action('wp_head', 'wpb_hook_javascript');

class JwtExample {

    function init()
    {
        add_action('wp_enqueue_scripts', [$this, 'enqueueScripts']);
        add_action('wp_head', [$this, 'outputForm']);
    }

    function enqueueScripts()
    {
        wp_enqueue_script('jwt-example', plugins_url('login.js', __FILE__), ['jquery']);
    }

    function outputForm()
    {
        require_once('login.html');
    }
}

$example = new JwtExample();
$example->init();
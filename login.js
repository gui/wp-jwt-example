jQuery(function() {

    function showResult(html) {
        document.getElementById('loginResult').innerHTML = html;
    }

    document.getElementById('loginSubmitBtn').addEventListener('click', function() {
    
        jQuery.post('/wp-json/jwt-auth/v1/token', {
            username: document.getElementById('username').value,
            password: document.getElementById('password').value,
        })
        .done(function(response) {
            showResult('Token: ' + response.token);
        })
        .fail(function(error) {
            showResult('Error: ' + error.responseJSON.message);
        });
    });
});